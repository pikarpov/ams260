# -*- coding: utf-8 -*-
"""
1D Euler

-PK
"""

import numpy as np
import matplotlib.pyplot as plt

class Primitive:
    def __init__(self, rho, u, p):
        self.rho = rho
        self.u = u
        self.p = p

def main():
    global gamma
    
    N = 128
    Ngc = 2
    gamma = 1.4
    #CFL = 0.9
    
    #x = 0.0 #???
    
    size = 2*Ngc+N
    gr_V = np.zeros([size,3])
    gr_U = np.zeros([size,3])
    gr_F = np.zeros([size,3])
    gr_U_n = np.zeros([size,3])

    #--
    dx = 0.01
    dt = 0.009
    #x = (i-1/2)*dx
    #t = n*dt
    
    #x_h = x+dt/2
    tmax = dt*0
    t=0
    while t<=tmax:
        gr_V, gr_U, gr_F = grid(N, Ngc, gr_V, gr_U, gr_F, t)
        #print(gr_U[:,:])
        
        gr_U = Godunov(gr_V, gr_U, gr_F, gr_U_n, N, Ngc, dt, dx)
        #print(gr_U[:,:])
        t+=dt
    print(gr_U[0])
    
    #plot
    x = np.linspace(0, 1, 128+4)
    U = []
    for i in range(len(gr_U)):
        U.append(gr_U[i][0])
    plt.figure(figsize=(10,10))
    plt.plot(x, U)


def grid(N, Ngc, gr_V, gr_U, gr_F, t):

    if t == 0:
        start = 0
        end = 2*Ngc+N
    else:
        start = Ngc
        end = N+Ngc
    for i in range(start, end):
        gr_V[i], gr_U[i], gr_F[i] = Params(t, i/N,    gr_U[i])
        
    return gr_V, gr_U, gr_F


def Params(t, x,    U):    
    if t==0:
        V = Initial(x)
        
        #костыль
        V_var = [V.rho, V.u, V.p]
        U = [V.rho, V.rho*V.u, E(V.rho, V.u, V.p, gamma)]
    else:
        rho = U[0]
        u = U[1]/U[0]
        p = U[2]/rho-u**2/2*(gamma-1)*rho
        V = Primitive(rho, u, p)
        #костыль
        V_var = [V.rho, V.u, V.p]
    
    F = [V.rho*V.u, V.rho*V.u**2+V.p, V.u*(V.p+E(V.rho, V.u, V.p, gamma))]
    
    return V_var, U, F


def Initial(x):
    if x <= 0.5:
        V = Primitive(1.0, 0.0, 1.0)
    else:
        V = Primitive(0.125, 0.0, 0.1)   
    return V


def Godunov(gr_V, gr_U, gr_F, gr_U_n, N, Ngc, dt, dx):
    gr_U_n=gr_U
    for i in range(Ngc, N+Ngc):
        
        #----------------------------
        #!!!CLEAN UP, REBUILD TO MODULAR
        #calculate F at i+1/2 and i-1/2
        F = []
        for k in range(2):
            #VL = Primitive(gr_V[i-1-k][0], gr_V[i-1-k][1], gr_V[i-1-k][2])
            #VR = Primitive(gr_V[i+1-k][0], gr_V[i+1-k][1], gr_V[i+1-k][2])
            
            VR = Primitive(gr_V[i+1-k][0], gr_V[i+1-k][1], gr_V[i+1-k][2])
            VL = Primitive(gr_V[i-k][0], gr_V[i-k][1], gr_V[i-k][2])
            
            F.append(hll(VL, VR, gr_U[i-1-k], gr_U[i+1-k], gr_F[i-1-k], gr_F[i+1-k]))
        #----------------------------  
        #advance to the next step
        for j in range(3):
            #gr_U_n[i][j]=gr_U[i][j] - dt/dx * (F[0][j]-F[1][j])
            gr_U[i][j] -= dt/dx * (F[0][j]-F[1][j])
    return gr_U

def hll(VL, VR, UL, UR, FL, FR):
    
    #sound speed from left and right
    aL = np.sqrt(gamma*VL.p/VL.rho)
    aR = np.sqrt(gamma*VR.p/VR.rho)

    #shock velocities (???)
    sL = min(VL.u - aL, VR.u - aR)
    sR = max(VL.u + aL, VR.u + aR)

    F = []
    
    for i in range(len(UL)):
        if sL >= 0:
            F.append(FL[i])
        elif sL < 0 and sR >= 0:
            F.append((sR*FL[i]-sL*FR[i]+sL*sR*(UR[i]-UL[i]))/(sR-sL))
        else:
            F.append(FL[i])
    
    return F

def E(rho, u, p, gamma):
    e = p/((gamma-1)*rho)
    E = rho*(u**2/2+e)
    return E
    
if __name__=='__main__':
    main()
    
'''
class Conserv:
    def __init__(self, rho, u, p, gamma):
        self.rho = rho
        self.URho = rho*u
        e = p/((gamma-1)*rho)
        self.E = rho*(u**2/2+e)

class Flux:
    def __init__(self, rho, u, p, gamma):
        self.one = rho*u
        self.two = rho*u**2+p
        e = p/((gamma-1)*rho)
        E = rho*(u**2/2+e)
        self.three = u*(E+p)
'''
    #u = Conserv()
    #for attr, value in u.__dict__.items():
    #    print(attr, value)
    #for attr, value in vars(u).items():
    #    print(attr, '=', value)
'''
    UL = Conserv(VL.rho, VL.u, VL.p, gamma)
    UR = Conserv(VR.rho, VR.u, VR.p, gamma)
    FL = Flux(VL.rho, VL.u, VL.p, gamma)
    FR = Flux(VR.rho, VR.u, VR.p, gamma)
'''
    #UL = [VL.rho, VL.rho*VL.u, E(VL.rho, VL.u, VL.p, gamma)]
    #UR = [VR.rho, VR.rho*VR.u, E(VR.rho, VR.u, VR.p, gamma)]
    #FL = [VL.rho*VL.u, VL.rho*VL.u**2+VL.p, VL.u*(VL.p+E(VL.rho, VL.u, VL.p, gamma))]
    #FR = [VR.rho*VR.u, VR.rho*VR.u**2+VR.p, VR.u*(VR.p+E(VR.rho, VR.u, VR.p, gamma))]
    
        #VL = Primitive(1.0, 0.0, 1.0)
    #VR = Primitive(0.125, 0.0, 0.1)
    #print(VL.rho, VR.rho) 
    
'''
    #rho, u, p = init(x)
    e = p/((gamma-1)*rho)
    E = rho*(u**2/2+e)
    Ueq = [rho, rho*u, E]
    Feq = [rho*u, rho*u**2+p, u*(E+p)]
'''